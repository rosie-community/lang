# lang
Rosie Pattern Language patterns for extracting features from source code in various languages

# How to use an RPL pattern repository

Clone the repository, and add it to your Rosie `libpath`.  Detailed instructions
follow. 

## The Rosie libpath

Rosie's command line looks for `rpl` pattern libraries in a list of directories
called the `libpath`.  After you clone a repository like this one, which contains
`rpl` pattern libraries, you need to add this directory to the Rosie `libpath`.

This can be done on the command line, or automatically by creating a
`~/.rosierc` file.

In either case, you **must** include the Rosie standard library directory if you
want to keep using those patterns.  The `libpath` is searched in order, so
typically the standard library is listed first, unless you deliberately wish to
shadow some of the standard libraries.

## Finding the current libpath

The `rosie config` command lists the configuration of the Rosie installation,
including `ROSIE_LIBDIR`, which points to the standard library, and
`ROSIE_LIBPATH`, which contains the current `libpath` value.

```shell 
$ rosie config
       ROSIE_VERSION = "1.0.0-beta-12" (set by distribution)
          ROSIE_HOME = "/usr/local/Cellar/rosie/1.0.0-beta-12/lib/rosie" (set by build)
        ROSIE_LIBDIR = "/usr/local/Cellar/rosie/1.0.0-beta-12/lib/rosie/rpl" (set by build)
       ROSIE_COMMAND = "rosie" (set by CLI)
         RPL_VERSION = "1.2" (set by distribution)
       ROSIE_LIBPATH = "/usr/local/Cellar/rosie/1.0.0-beta-12/lib/rosie/rpl" (set by default)
              colors = "*=default;bold:net.*=red:net.ipv6=red;underline:net.path=green:net.MAC=underline;green:num.*=underline:word.*=yellow:all.identifier=cyan:id.*=bold;cyan:os.path=green:date.*=blue:time.*=1;34:ts.*=underline;blue" (set by default)

Build log: /usr/local/Cellar/rosie/1.0.0-beta-12/lib/rosie/build.log
$ 
``` 

In the example above, Rosie was installed on a Macbook using
[brew](https://gitlab.com/rosie-community/homebrew-rosie), which is why some
paths contain `Cellar`.

## Creating an initial ~/.rosierc file

Now let's extract the value of `ROSIE_LIBDIR` so we can use it to create our
`~/.rosierc` file.

```shell 
$ rosie config | rosie grep '"ROSIE_LIBDIR"'
        ROSIE_LIBDIR = "/usr/local/Cellar/rosie/1.0.0-beta-12/lib/rosie/rpl" (set by build)
$ rosie config | rosie grep '"ROSIE_LIBDIR"' | rosie grep word.dq
        ROSIE_LIBDIR = "/usr/local/Cellar/rosie/1.0.0-beta-12/lib/rosie/rpl" (set by build)
$ rosie config | rosie grep '"ROSIE_LIBDIR"' | rosie grep -o subs word.dq
"/usr/local/Cellar/rosie/1.0.0-beta-12/lib/rosie/rpl"
$ 
``` 

In the second command above, we did not tell Rosie to output the submatches, so
by default the output was the entire line (which is the default for `rosie
grep`).  The last line adds `-o subs` to create the output we want.

The pattern `word.dq` captures a double-quoted string.

Let's use this to create our `~/.rosierc`, assuming we do not have one already.

```shell 
$ echo 'libpath = ' $(rosie config | rosie grep '"ROSIE_LIBDIR"' | rosie grep -o subs word.dq) > ~/.rosierc
$ cat ~/.rosierc
libpath =  "/usr/local/Cellar/rosie/1.0.0-beta-12/lib/rosie/rpl"
$ 
``` 

## Adding a new directory to the libpath

The command below will add the current directory to your `libpath`.  If the
current directory contains `python.rpl`, then after executing this command, you
will be able to use (for example) `python.line_comment` on the Rosie command
line. 

```shell 
echo libpath = \"$(pwd)\" >> ~/.rosierc
```

Note that this command includes escaped quotes because values in `~/.rosierc`
are quoted.  Also, Rosie permits multiple declarations of `libpath` in the
file.  The various values will be concatenated.  This can be verified:

```shell 
$ rosie config | rosie grep '"ROSIE_LIBPATH"'
       ROSIE_LIBPATH = "/usr/local/Cellar/rosie/1.0.0-beta-12/lib/rosie/rpl:/Users/jennings/Projects/community/lang" (set by rcfile)
$ 
``` 

# How to use this repository

Most of the files here look for multi-line patterns, and so using them from the
command line requires the `--wholefile` option.  When using these patterns from
Python or another programming language, simply supply the entire input text as a
single string argument to Rosie.

Example:

```shell 
$ rosie match --wholefile -o subs bash.strings ~/Projects/rosie-pattern-language/rosie/extra/docker/run 
"$dockerfile"
""
"Usage $0 <name-of-dockerfile> [branch] [fresh]"
"$branch"
""
"No branch argument (will checkout master)"
"master"
''
"$3"
"fresh"
'Building a fresh image using --no-cache'
'--no-cache'
$ 
``` 



