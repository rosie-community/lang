-- -*- Mode: rpl; -*-                                                                               
--
-- cpp.rpl
--
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHORS: Jordan Connor, Trevor Gasdaska, Spatika Ganesh, Maya Shankar, Jamie A. Jennings

package cpp

-- N.B. The --wholefile option is needed for many of these patterns because they
-- can span lines.

alias line_end = "\n"

-------------------------------
--String Literals
--
--Currently matches for typical c string and character literals.
--Does allow for escaped single and double quotes
--A "aliased" version is provided to allow for suppressed output in other patterns
-- Run: rosie -wholefile strings <cpp-source-file>
-- Structure: strings                      // Parent pattern to extract all strings from file
--              \- string                  // Matches a single cpp string
--            characters                   // Parent pattern to match all character literals from file
--              \- character               // Matches a single cpp character
-- NOTE strings and characters are defined AFTER comments to allow for
--      commented strings to be ignored.
-------------------------------
alias alias_string = ["] {([\\] / !["]) .}* ["]
string = ["] {([\\] / !["]) .}* ["]

alias alias_character = ['] {([\\] / ![']) .} [']
character =  ['] {([\\] / ![']) .} [']

-------------------------------
-- Inline Comments
--
-- Matches in line comments starting with a '//'. Also
-- captures the code preceding the comment on the same line.
--
-- Run: rosie -wholefile line_comments <source-file>
-- Structure: line_comments                      // Parent pattern to extract all comments from file
--                \- line_comment                // Matches a single line comment
--                    \- line_comment_context    // The code on the line preceding the comment
--                    \- line_comment_body       // Matches the whole comment text
--                        \- line_comment_text   // The text of the comment
-------------------------------
alias line_comment_start = "//"
line_comment_text = {!line_end .}*
alias line_comment_pre = {!line_comment_start !line_end .}*
line_comment_context = {!line_comment_start !line_end .}*
line_comment_body = {line_comment_start line_comment_text}
aliased_line_comment = {line_comment_context line_comment_body}
line_comment = {line_comment_context line_comment_body}
line_comments = {line_comment / .}*

alias alias_line_comment_start = "//"
alias alias_line_comment_text = {!line_end .}*
alias alias_line_comment_pre = {!alias_line_comment_start !line_end .}*
alias alias_line_comment_context = {!alias_line_comment_start !line_end .}*
alias alias_line_comment_body = {alias_line_comment_start alias_line_comment_text}
alias alias_line_comment = {alias_line_comment_context alias_line_comment_body}
alias alias_line_comments = {{alias_line_comment / alias_line_comment_body / alias_line_comment_pre} .}*


-------------------------------
-- Block Comments
--
-- Matches block comments. Block comments in c
-- start with /* and end with */
--
-- Run: rosie -wholefile block_comments <source-file>
-- Structure: block_comments                  // Parent pattern to match all block comments
--                \- block_comment            // Matches a single block comment
--                    \- block_comment_body   // Matches the body of a block comment
-------------------------------
alias block_comment_start = "/*"
alias block_comment_end = "*/"
alias block_comment_pre = {!block_comment_start .}*
block_comment_body = {!block_comment_end .}*
aliased_block_comment = { block_comment_start block_comment_body block_comment_end}
block_comment = { block_comment_start block_comment_body block_comment_end}

block_comments = {block_comment_pre block_comment }*

alias alias_block_comment_start = "/*"
alias alias_block_comment_end = "*/"
alias alias_block_comment_pre = {!block_comment_start .}*

alias alias_block_comment_body = {!alias_block_comment_end .}*
alias alias_block_comment = { alias_block_comment_start alias_block_comment_body alias_block_comment_end}

alias alias_block_comments = {alias_block_comment_pre alias_block_comment }*

strings = {alias_line_comment / alias_block_comment / string / .}*
characters = {alias_line_comment / alias_block_comment / character / .}*

-------------------------------
-- Dependencies
--
-- Matches dependencies declared with "include" 
--
-- Run: rosie -wholefile dependencies <source-file>
-- Structure: dependencies                  // Parent pattern to match all dependencies in a file
--                \- dependency             // Matches a single import
-------------------------------
alias include = "#include "
header = {{!">" !["]} .}*
alias import_lit = {"<" / ["]} header
dependency = {include import_lit {">" / ["]}}
alias dependencies_pre = {!include !line_end .}*

dependencies = {{dependency / dependencies_pre} line_end}*

-------------------------------
-- Functions
--
-- Pattern to match function definitions. This does not
-- capture the functions body. This is currently the same syntatic definition of a C++ function.
-- New features of C++ have not yet been accounted for.
--
-- Run: rosie -wholefile functions <cpp-source-file>
-- Structure: functions                       // Parent pattern that matches all function definitions
--                \- function                 // Matches an entire function definition
--                        \- static           // Matches if the function call is defined as static
--                        \- return_type      // Matches the return type of the function
--                        \- pointer          // Matches wether or not the functions return type is a pointer
--                        \- function_name    // Matches the functions defined name
--                        \- parameters       // Matches the functions list of parameters
--                            \- single_param // Matches a single parameter in a function call.
--------------------------------
alias seperator = ","
alias start_paren = "("
alias end_paren = ")"
alias start_block = "{"
alias end_block = "}"
alias pointer = "*"
alias invalid_identifier = [^ [A-Z][a-z][0-9][_][&][<][>]]
alias keywords = {"if" / "for" / "switch" / "catch" / "try" / "do" / "while" / "else" / "else if" / "} else if"}
alias whitespace = [:space:]
alias const = "const"
static = "static"
inline = "inline"
return_type = !keywords const? "struct"? {!invalid_identifier .}+ pointer*
function_name = !keywords {!line_end !start_paren .}+

single_param = {!end_paren ![,] .}*
alias multi_param = {[,] single_param}*
parameters = {start_paren single_param multi_param end_paren whitespace+}

function = whitespace? static? inline? return_type !keywords function_name !keywords parameters const? "{"
functions = {function / .}*

-------------------------------
-- Structs/Class
--
-- Pattern to match struct definitions. Does not
-- capture struct bodies.
--
-- Run: rosie -wholefile [structs | classes] <c++-source-file>
-- Structure: structs | classes                         // Parent pattern that matches all struct/class definitions
--                \- struct | class                     // Matches an entire struct/class definition
--                    \- struct_name | class_name       // Name of the struct/class
-------------------------------

alias typedef = "typedef "
alias struct_decl = "struct " 
alias enum = "enum"
alias class_decl = {whitespace "class" whitespace}
type = {{!invalid_identifier .}+ " "}+

struct_name = {!start_block .}*
class_name = {!start_block .}*

struct = typedef? struct_decl type?  struct_name
class = {enum? class_decl type? class_name}
structs = {{alias_line_comment / alias_block_comment}? struct / .}*
classes = {{alias_line_comment / alias_block_comment}? class / .}*

file = {block_comment / line_comment / dependency / struct / class / function / .}*